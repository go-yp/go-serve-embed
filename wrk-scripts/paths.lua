-- original https://github.com/wg/wrk/blob/master/scripts/counter.lua

-- tree public
-- public/
-- ├── assets
-- │   └── js
-- │       └── test.js
-- ├── favicon-16x16.png
-- ├── favicon-32x32.png
-- ├── favicon.ico
-- ├── index.html
-- └── test.html

local paths = {
    "/assets/js/test.js",
    "/favicon-16x16.png",
    "/favicon-32x32.png",
    "/favicon.ico",
    "/index.html",
    "/test.html",
    "/"
}
local len = #paths
local counter = 0

request = function()
   counter = (counter + 1) % len

   return wrk.format(nil, paths[1+counter])
end
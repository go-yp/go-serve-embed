package main

import (
	"encoding/base64"
	"gitlab.com/go-yp/go-serve-embed/api"
	"log"
	"net/http"
)

var (
	GitHash      string
	indexHTML    string
	testHTML     string
	favicon16PNG string
	favicon32PNG string
	faviconICO   string
	testJS       string
)

func main() {
	http.HandleFunc("/stopdiiacity/verify.json", api.VerifyHandler)

	http.Handle("/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch r.URL.Path {
		case "/", "/index.html":
			w.Write([]byte(indexHTML))
		case "/test.html":
			w.Write([]byte(testHTML))
		case "/favicon-16x16.png":
			base64Decode(w, favicon16PNG)
		case "/favicon-32x32.png":
			base64Decode(w, favicon32PNG)
		case "/favicon.ico":
			base64Decode(w, faviconICO)
		case "/assets/js/test.js":
			w.Write([]byte(testJS))
		case "/git-hash":
			w.Write([]byte(GitHash))
		default:
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte(http.StatusText(http.StatusNotFound)))
		}
	}))

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal(err)
	}
}

func base64Decode(w http.ResponseWriter, data string) {
	var content, err = base64.StdEncoding.DecodeString(data)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(http.StatusText(http.StatusInternalServerError)))

		return
	}

	w.Write(content)
}

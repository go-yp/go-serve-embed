package stopdiiacity

import (
	"strings"
)

var stopDiiaCityPrefixes = []string{
	"https://jobs.dou.ua/companies/epam-systems/",
	"https://jobs.dou.ua/companies/intetics-co/",
	"https://jobs.dou.ua/companies/genesis-technology-partners/",
	"https://jobs.dou.ua/companies/parimatch-tech/",
	"https://jobs.dou.ua/companies/ajax-systems/",
}

func Contains(requestURLs []string) bool {
	for _, url := range requestURLs {
		for _, prefix := range stopDiiaCityPrefixes {
			if strings.HasPrefix(url, prefix) {
				return true
			}
		}
	}

	return false
}

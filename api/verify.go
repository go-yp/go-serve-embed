package api

import (
	"encoding/json"
	"gitlab.com/go-yp/go-serve-embed/stopdiiacity"
	"io/ioutil"
	"net/http"
)

type Request struct {
	URLs []string `json:"urls"`
}

func VerifyHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Access-Control-Allow-Origin", "*")
	w.Header().Add("Content-Type", "application/json")

	var requestContent, err = ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)

		return
	}

	var responseBody, responseStatusCode = verify(requestContent)
	w.WriteHeader(responseStatusCode)
	w.Write(responseBody)
}

func verify(content []byte) ([]byte, int) {
	var request Request

	var unmarhsalErr = json.Unmarshal(content, &request)
	if unmarhsalErr != nil {
		// TODO: add error message
		return []byte(`{}`), http.StatusBadRequest
	}

	if len(request.URLs) == 0 {
		// TODO: add error message
		return []byte(`{}`), http.StatusBadRequest
	}

	if stopdiiacity.Contains(request.URLs) {
		return []byte(`{"exists":true}`), http.StatusOK
	}

	return []byte(`{"exists":false}`), http.StatusOK
}

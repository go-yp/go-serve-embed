{
    const $form = document.getElementById("js-form");

    const $url = $form.elements["url"];

    $form.onsubmit = function () {
        const url = $url.value;

        fetch("/stopdiiacity/verify.json", {
            method: "POST",
            body: JSON.stringify({
                "urls": [url],
            }),
        }).then(function (response) {
            return response.json();
        }).then(function (json) {
            if (json.exists) {
                alert(`URL ${url} is StopDiiaCity!`);
            } else {
                alert(`URL ${url} is safe!`);
            }
        }).catch(console.error)
    }
}
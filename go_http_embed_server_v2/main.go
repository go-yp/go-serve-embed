package main

import (
	"embed"
	"gitlab.com/go-yp/go-serve-embed/api"
	"io/fs"
	"log"
	"net/http"
)

//go:embed public
var publicFS embed.FS

func main() {
	http.HandleFunc("/stopdiiacity/verify.json", api.VerifyHandler)

	http.Handle("/", http.FileServer(mustPublicFS()))

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal(err)
	}
}

func mustPublicFS() http.FileSystem {
	sub, err := fs.Sub(publicFS, "public")

	if err != nil {
		panic(err)
	}

	return http.FS(sub)
}

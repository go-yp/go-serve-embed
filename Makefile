wrk:
	wrk -t 2 -c 1000 http://127.0.0.1:8080/
	wrk -t 2 -c 1000 http://127.0.0.1:8081/
	wrk -t 2 -c 1000 http://127.0.0.1:8082/

wrk-all:
	wrk -s ./wrk-scripts/paths.lua -t 2 -c 1000 http://127.0.0.1:8080/
	wrk -s ./wrk-scripts/paths.lua -t 2 -c 1000 http://127.0.0.1:8081/
	wrk -s ./wrk-scripts/paths.lua -t 2 -c 1000 http://127.0.0.1:8082/
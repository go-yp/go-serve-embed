package main

import (
	"gitlab.com/go-yp/go-serve-embed/api"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/stopdiiacity/verify.json", api.VerifyHandler)

	http.Handle("/", http.FileServer(http.Dir("/var/www/public")))

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal(err)
	}
}
